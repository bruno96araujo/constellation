﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CreateLine : MonoBehaviour {

	// Creates a line renderer that follows a Sin() function
	// and animates it.

	//public Color c1 = Color.yellow;
	//public Color c2 = Color.red;
	//public int lengthOfLineRenderer = 20;

	[SerializeField]
	private GameObject lineRenderer;

	LineRenderer lineRendererTwo;
	public float counter;
	public float dist;
	public float lineDrawSpeed = 0.6f;

	private void InitCreateLine(int id, Transform origin, Transform destination){
		GameObject lineUP = Instantiate(lineRenderer);
		lineRendererTwo = lineUP.GetComponent<LineRenderer>();
		lineRendererTwo.SetPosition(id, origin.position);
		Debug.Log("45: "+id);
		lineRendererTwo.startWidth = 1.0f;
		
		//lineRendererTwo.positionCount = 4;

		dist = Vector3.Distance (origin.position, destination.position);

		lineRendererTwo.enabled = true;
	}

	public void SetLine(int id, Transform origin, Transform destination) {
		InitCreateLine(id, origin, destination);
		//lineRenderer.enabled = true;
		SetPositionLine(id, origin,destination);
	}

	private void SetPositionLine(int id, Transform origin, Transform destination){
		while (counter < dist) {
			counter += .1f / lineDrawSpeed;
			float x = Mathf.Lerp (0, dist, counter);

			Vector3 pointA = origin.position;
			Vector3 pointB = destination.position;

			Vector3 pointLong = x * Vector3.Normalize (pointB - pointA) + pointA;


			lineRendererTwo.SetPosition(id, pointLong);

		}
	}
}