﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class StarsGraphics : MonoBehaviour
{

    private List<Transform> GameTransform;
    public GameObject parent;
    Dictionary<string, Transform> starsGroup = new Dictionary<string, Transform>();
    TesteLine CreateLineObj;
    int x = 0;

    void Start()
    {
        GameTransform = new List<Transform>();
        CreateLineObj = GameObject.Find("GroupStars").GetComponent<TesteLine>();
        for (int i = 0; i < 120; i++)
        {
            LoadStars(Random.Range(-300, 300), Random.Range(-300, 300), Random.Range(-300, 300));
        }
        CreateLineStar();
    }
    public void LoadStars(int x, int y, int z)
    {
        Transform Stars = Instantiate(Resources.Load<Transform>("Stars"), parent.transform);
        Stars.transform.gameObject.name = "Stars_" + (Stars.GetInstanceID() * (-1)); //Rename GameObject Stars
        Stars.localPosition = new Vector3(x, y, z); // Set position of the gameObject
        GameTransform.Add(Stars);
        starsGroup.Add(Stars.transform.gameObject.name, Stars); // Add gameObject in dictionary
    }

    private void CreateLineStar()
    {
		
		
    }

}