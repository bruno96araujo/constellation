﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GetLabelText : MonoBehaviour {

	public Text titleStars;
	public Text descriptionTitle;
	public GameObject screen;
	bool Active = false;
	private Image image;

	void Start(){
		image = screen.GetComponent<Image>();
	}

	public void SetTextLabel(string title, string description){
		titleStars.text = title;
		descriptionTitle.text = description;
		image.enabled = !Active;
		StartCoroutine(CloseScreen());
	}

	IEnumerator CloseScreen(){
		yield return new WaitForSeconds(10);
		image.enabled = Active;
		titleStars.text = "";
		descriptionTitle.text = "";
	}

}
