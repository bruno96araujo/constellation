﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SetPositionsGameObject : MonoBehaviour {
	GameObject gamer;
	public Text redColor;
	public Text blueColor;
	public Text greenColor;
	public Slider slideRed;
	public Slider slideGreen;
	public Slider slideBlue;
	public Slider activateIntensity;

	float x = 10;
	float y = 10;
	float z = 10;
	ColorSelector SetNewColor;
	void Start () {
		SetNewColor = GameObject.Find ("_ColorSelector").GetComponent<ColorSelector> ();
	}
	void Update () {
		UpdateValueColor ();
	}
	public void SetPositionX () {
		if (gamer != null) {
			gamer.transform.localPosition = new Vector3 (gamer.transform.localPosition.x + x, gamer.transform.localPosition.y, gamer.transform.localPosition.z);
		}
	}

	public void SetPositionY () {
		if (gamer != null) {
			gamer.transform.localPosition = new Vector3 (gamer.transform.localPosition.x, gamer.transform.localPosition.y + y, gamer.transform.localPosition.z);
		}
	}

	public void SetPositionZ () {
		if (gamer != null) {
			gamer.transform.localPosition = new Vector3 (gamer.transform.localPosition.x, gamer.transform.localPosition.y, gamer.transform.localPosition.z + z);
		}
	}

	public void SetLightIntensity () {
		gamer.GetComponent<Light> ().intensity = activateIntensity.value;
	}

	public void SetGameObject (GameObject game) {
		gamer = game;
	}

	public void ChangeColor (Color color) {
		Renderer rend = gamer.GetComponent<Renderer> ();
		rend.material.SetColor ("_Color", color);
		rend.material.color = color;
	}

	public void SetChangedColorPicker () {
		ChangeColor (SetNewColor.colorNew);
	}

	public void SetChangedColor () {
		Color newColor = new Color (slideRed.value, slideGreen.value, slideBlue.value, 1.0f);
		ChangeColor (newColor);
	}

	public void UpdateValueColor () {
		if (gamer != null) {
			redColor.text = slideRed.value.ToString ();
			greenColor.text = slideGreen.value.ToString ();
			blueColor.text = slideBlue.value.ToString ();

			SetLightIntensity ();
		}
	}

	public void Inverse () {
		x = (x * (-1));
		y = (y * (-1));
		z = (z * (-1));
	}
}