﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GetInstanceGameObject : MonoBehaviour {
    GetLabelText labelText;
    SetPositionsGameObject getGameObject;
    void Start () {
        labelText = GameObject.Find ("DatasStars").GetComponent<GetLabelText> ();
        getGameObject = GameObject.Find ("DatasStars").GetComponent<SetPositionsGameObject> ();
    }
    public void OnMouseDown () {
        if (Input.GetButtonDown ("Fire1")) {
            getGameObject.SetGameObject (gameObject);
            labelText.SetTextLabel (gameObject.name.ToString (), gameObject.GetInstanceID ().ToString ());
        }
    }
}