﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LoadImages : MonoBehaviour
{

    public Transform parent;

    void Start()
    {
        for (int i = 0; i < 5; i++)
        {
            StartCoroutine(setImage("https://i-h2.pinimg.com/564x/91/dd/7d/91dd7d0cd80413f2932b46898c08061a.jpg"));
        }
    }
    IEnumerator setImage(string urlImage)
    {
        Debug.Log("TreeInstance");
        using (WWW www = new WWW(urlImage))
        {
            yield return www;


            Image img = Instantiate(Resources.Load<Image>("ImageView"), parent.transform);

            img.sprite = Sprite.Create(www.texture, new Rect(0, 0, www.texture.width, www.texture.height), new Vector2(0, 0));


            Debug.Log("Download: " + www.progress);


        }

    }
}
