﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ConstellationController : MonoBehaviour
{

    public Button buttonImport;
    public Button buttonVisualize;
    public Button buttonOther;
    public Button CloseVisualize;
    public Button CloseOther;
    public GameObject ScreenFileBrowser;
    public GameObject ScreenVisualize;
    public GameObject ScreenOther;
    bool boolClickMenu = false;

    // Use this for initialization
    void Start()
    {
        Button btn = buttonImport.GetComponent<Button>();
        btn.onClick.AddListener(ClickActionImport);

        Button btn_two = buttonVisualize.GetComponent<Button>();
        btn_two.onClick.AddListener(ClickActionVisualize);

        Button btn_five = buttonOther.GetComponent<Button>();
        btn_five.onClick.AddListener(ClickActionOther);

        Button btn_three = CloseVisualize.GetComponent<Button>();
        btn_three.onClick.AddListener(CloseVisualizeScreen);

        Button btn_four = CloseOther.GetComponent<Button>();
        btn_four.onClick.AddListener(CloseOtherScreen);
    }

    // Update is called once per frame
    void Update()
    {

    }

    void ClickActionImport()
    {
        ScreenVisualize.SetActive(boolClickMenu);
        ScreenFileBrowser.SetActive(!boolClickMenu);
		ScreenOther.SetActive(boolClickMenu);
    }

    void ClickActionVisualize()
    {
        ScreenFileBrowser.SetActive(boolClickMenu);
        ScreenVisualize.SetActive(!boolClickMenu);
		ScreenOther.SetActive(boolClickMenu);
    }
    void ClickActionOther()
    {
        ScreenFileBrowser.SetActive(boolClickMenu);
        ScreenVisualize.SetActive(boolClickMenu);
		ScreenOther.SetActive(!boolClickMenu);
    }
    void CloseVisualizeScreen()
    {
        ScreenVisualize.SetActive(boolClickMenu);
    }
    void CloseOtherScreen()
    {
        ScreenOther.SetActive(boolClickMenu);
    }
}
