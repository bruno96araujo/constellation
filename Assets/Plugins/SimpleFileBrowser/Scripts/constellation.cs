﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class constellation : MonoBehaviour {

	#region Variables
	string path_url;
	#endregion	

	#region VariablesUnity
	public GameObject Loading;
	public Slider Slider;
	#endregion	
	
	public void LoadLevel(int sceneIndex)
    {
		StartCoroutine(AsynchronousLoad(sceneIndex));
    }

    IEnumerator AsynchronousLoad(int sceneIndex)
    {

        AsyncOperation ao = SceneManager.LoadSceneAsync(sceneIndex);
		Loading.SetActive(true);
        while (!ao.isDone)
        {
			float progress = Mathf.Clamp01(ao.progress / .9f);
			Slider.value = progress;
            Debug.Log("Loading progress: "+ao.progress);
            yield return null;
        }
    }

	public void Set_Path_Directory(string path){
		path_url = path;
		Debug.Log("MyPath: "+path_url);
		LoadLevel(0);
	}
}
